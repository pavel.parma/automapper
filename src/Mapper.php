<?php
namespace Sapientes\Automapper;


/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Mapper {
	/** @var  MappingStrategy\Strategy */
	protected $mappingStrategy;
    /** @var SanitizingStrategy\Strategy */
    protected $sanitizingStrategy;
    
    /**
     * Mapper constructor.
     *
     * @param MappingStrategy\Strategy    $mappingStrategy
     * @param SanitizingStrategy\Strategy $sanitizingStrategy
     */
	public function __construct(MappingStrategy\Strategy $mappingStrategy, SanitizingStrategy\Strategy $sanitizingStrategy = null) {
		$this->mappingStrategy    = $mappingStrategy;
        $this->sanitizingStrategy = $sanitizingStrategy;
    }
	
	/**
	 * Map source into destination
	 *
	 * @param mixed  $source
	 * @param string $destination
	 *
	 * @return mixed
	 */
	public function map($source, string $destination) {
		$reflectionClass = new \ReflectionClass($destination);
		return $this->buildObject($destination, $this->mapProperties(new Source($source), $reflectionClass));
	}
    
    /**
     * Build object with picked values.
     *
     * @param string $destination
     * @param array  $values
     *
     * @return object
     */
	protected function buildObject(string $destination, array $values) {
	    $object = new $destination();
        foreach (array_keys(get_object_vars($object)) as $classProperty) {
            if(array_key_exists($classProperty, $values)) {
                $object->$classProperty = $values[$classProperty];
            }
        }
        
        return $object;
    }
    
    /**
     * Map properties.
     *
     * @param  Source          $source
     * @param \ReflectionClass $destination
     *
     * @return array
     */
	protected function mapProperties(Source $source, \ReflectionClass $destination) : array {
		$result = [];
		foreach ($destination->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
		    if(! $this->mappingStrategy->shouldMap($source, $property)) continue;
            
            $value                        = $this->mappingStrategy->getValue($source, $property, $this);
            $result[$property->getName()] = is_null($this->sanitizingStrategy) ? $value : $this->sanitizingStrategy->sanitize($value, $property);
		}
		
		return $result;
	}
}