<?php
namespace Sanitizer\String;

use Sapientes\Automapper\Annotations\Sanitizer\AnnotationTrait;
use Sapientes\Automapper\Annotations\Sanitizer\SanitizerAnnotation;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 *
 * @AnnotationStrategy
 * @Target("PROPERTY")
 */
class LowerCase implements SanitizerAnnotation {
    
    use AnnotationTrait;
    
    /** @var  string */
    public $source;
    
    /**
     * @inheritdoc
     */
    public function getSanitizerName(): string {
        return \Sapientes\Automapper\Sanitizers\String\LowerCase::class;
    }
}