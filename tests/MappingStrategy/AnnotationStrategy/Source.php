<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy;


/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Source {
    /** @var PersonSource  */
    public $person;
    /** @var   */
    public $companies;
    
    /**
     * Source constructor.
     */
    public function __construct() {
        $this->person = new PersonSource('Person Name');
        $this->companies = [
            new CompanySource('First Company'),
            new CompanySource('Second Company')
        ];
    }
    
    public function getText() {
        return 'Just some text';
    }
    
    public function getObject() {
        return (object) ['property' => 'value'];
    }
}

