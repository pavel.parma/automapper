<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy;

use Sapientes\Automapper\Annotations\Mapping as Mapping;

class Destination {
    use DestinationConstruct;
    
    /**
     * @Mapping\Association(destination=PersonDestination::class, source="person")
     *
     * @var PersonDestination
     */
    public $person;
    
    /**
     * @Mapping\Associations(destination=CompanyDestination::class, source="companies")
     *
     * @var CompanyDestination[]
     */
    public $companies;
    
    /**
     * @Mapping\NoMap
     * @var int
     */
    public $noMap;
    
    /**
     * @Mapping\FromFunction(source=".",function="getText")
     *
     * @var string
     */
    public $text;
    
    /**
     * @Mapping\FromFunction(source=".",function="getObject")
     *
     * @var
     */
    public $object;
    
    /**
     * @Mapping\FromFunction(function="phpversion")
     * @var string
     */
    public $phpver;
}