<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy;

use Sapientes\Automapper\Annotations\Mapping as Mapping;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class PersonDestination {
    use DestinationConstruct;
    
    /**
     * @Mapping\FromMember(source="name")
     *
     * @var string
     */
    public $name;
    /**
     * @Mapping\FromFunction(source=".", function="getAge")
     *
     * @var string
     */
    public $age;
}