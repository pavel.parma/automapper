<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class CompanySource {
    public $name;
    
    /**
     * CompanySource constructor.
     *
     * @param $name
     */
    public function __construct($name) {
        $this->name = $name;
    }
}